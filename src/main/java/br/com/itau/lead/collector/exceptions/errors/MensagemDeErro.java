package br.com.itau.lead.collector.exceptions.errors;

import java.util.HashMap;

public class MensagemDeErro {
    private String erro;
    private String mensagem;
    private HashMap<String, ObjetoDeErro> camposDeErro;

    public MensagemDeErro(){ }

    public MensagemDeErro(String erro, String mensagem, HashMap<String, ObjetoDeErro> camposDeErro) {
        this.erro = erro;
        this.mensagem = mensagem;
        this.camposDeErro = camposDeErro;
    }

    public String getErro() {
        return erro;
    }

    public void setErro(String erro) {
        this.erro = erro;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public HashMap<String, ObjetoDeErro> getCamposDeErro() {
        return camposDeErro;
    }

    public void setCamposDeErro(HashMap<String, ObjetoDeErro> camposDeErro) {
        this.camposDeErro = camposDeErro;
    }
}
