package br.com.itau.lead.collector.models;

import br.com.itau.lead.collector.enums.TipoLeadEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

/*
@Table(name = "lead")
*/

@Entity
@JsonIgnoreProperties(value = {"data"}, allowGetters = true)
public class Lead {

//  @Column(name = "nome_lead")

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Size(min = 5, max = 100, message = "O nome deve ter de 5 a 100 caracteres.")
    @NotNull(message = "O nome não pode ser nulo.")
    private String nome;

    @Email(message = "O formato do e-mail está inválido.")
    @NotNull(message = "O e-mail não pode ser nulo")
    private String email;

    private LocalDate data;

    @NotNull(message = "O tipo de lead não pode ser nulo")
    private TipoLeadEnum tipoLead;

    @ManyToMany(cascade = CascadeType.ALL)
    private List<Produto> produtos;

    public Lead() { }

    public int getId(){
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getEmail() {
        return email;
    }

    public LocalDate getData() {
        return data;
    }

    public TipoLeadEnum getTipoLead() {
        return tipoLead;
    }

    public List<Produto> getProdutos() { return produtos; }

    public void setId(int id) { this.id = id; }

    public void setNome(String nome) { this.nome = nome; }

    public void setEmail(String email) { this.email = email; }

    public void setData(LocalDate data) { this.data = data; }

    public void setTipoLead(TipoLeadEnum tipoLead) { this.tipoLead = tipoLead; }

    public void setProdutos(List<Produto> produtos) { this.produtos = produtos; }

}
