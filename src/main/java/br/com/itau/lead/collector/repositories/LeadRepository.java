package br.com.itau.lead.collector.repositories;

import br.com.itau.lead.collector.enums.TipoLeadEnum;
import br.com.itau.lead.collector.models.Lead;
import org.springframework.data.repository.CrudRepository;

public interface LeadRepository extends CrudRepository<Lead, Integer> {
    Iterable<Lead> findAllByTipoLead(TipoLeadEnum tipoLeadEnum);
}
