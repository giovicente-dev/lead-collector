package br.com.itau.lead.collector.configurations;

import br.com.itau.lead.collector.security.FiltroAutenticacao;
import br.com.itau.lead.collector.security.FiltroAutorizacao;
import br.com.itau.lead.collector.security.JWTUtil;
import br.com.itau.lead.collector.services.LoginUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

/*  @CrossOrigin(origins =  "htpp://127.0.0.1", value = "/leads")                      */
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private JWTUtil jwtUtil;

    @Autowired
    private LoginUsuarioService loginUsuarioService;

    private static final String[] PUBLIC_MATCHERS_GET = {
            "/leads",
            "/leads/*",
            "/produtos"
    };

    private static final String[] PUBLIC_MATCHERS_POST = {
            "/leads",
            "/usuario",
            "/login"
    };

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable();

        http.authorizeRequests()
        .antMatchers(HttpMethod.GET, PUBLIC_MATCHERS_GET).permitAll()
        .antMatchers(HttpMethod.POST, PUBLIC_MATCHERS_POST).permitAll()
        .anyRequest().authenticated();

        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.addFilter(new FiltroAutenticacao(authenticationManager(), jwtUtil));
        http.addFilter(new FiltroAutorizacao(authenticationManager(), jwtUtil, loginUsuarioService));
    }

/*  se você não coloca a visibilidade do método ele por default assume que é protected */
    @Bean
    CorsConfigurationSource configuracaoDeCors() {
        final UrlBasedCorsConfigurationSource cors = new UrlBasedCorsConfigurationSource();
        cors.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
        return cors;
    }

    @Bean
    BCryptPasswordEncoder bCryptPasswordEncoder() { return new BCryptPasswordEncoder(); }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(loginUsuarioService).passwordEncoder(bCryptPasswordEncoder());
    }
}
