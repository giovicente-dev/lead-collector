package br.com.itau.lead.collector.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JWTUtil {

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.expiration}")
    private long expiration;

    public String gerarToken(String username) {

        String token = Jwts.builder()
                       .setSubject(username)
                       .setExpiration(new Date(System.currentTimeMillis() + expiration))
                       .signWith(SignatureAlgorithm.HS512, secret.getBytes())
                       .compact();

        return token;
    }

    public boolean tokenValido(String token) {
        try {
            Claims claims = getClaims(token);

            String email = claims.getSubject();

            Date expirationDate = claims.getExpiration();
            Date currentDate = new Date(System.currentTimeMillis());

            if (email != null && currentDate.before(expirationDate)) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            return false;
        }

    }

    public Claims getClaims(String token) {
        try {
            return Jwts.parser().setSigningKey(secret.getBytes()).parseClaimsJws(token).getBody();
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public String getUsername(String token) {
        Claims claims = getClaims(token);
        String username = claims.getSubject();
        return username;
    }

}
