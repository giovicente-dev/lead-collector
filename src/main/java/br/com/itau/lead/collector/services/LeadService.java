package br.com.itau.lead.collector.services;

import br.com.itau.lead.collector.enums.TipoLeadEnum;
import br.com.itau.lead.collector.models.Lead;
import br.com.itau.lead.collector.repositories.LeadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

@Service
public class LeadService {

    @Autowired
    private LeadRepository leadRepository;

    public Lead salvarLead(Lead lead) {
        LocalDate data = LocalDate.now();
        lead.setData(data);

        Lead leadObjeto = leadRepository.save(lead);
        return leadObjeto;
    }

    public Iterable<Lead> consultarListaLeads() {
        Iterable<Lead> leads = leadRepository.findAll();
        return leads;
    }

    public Iterable<Lead> consultarListaPorTipoDeLead(TipoLeadEnum tipoLeadEnum) {
        Iterable<Lead> leads = leadRepository.findAllByTipoLead(tipoLeadEnum);
        return leads;
    }

    public Lead consultarLeadPorId(int id) {
        Optional<Lead> optionalLead = leadRepository.findById(id);

        if (optionalLead.isPresent()) {
            return optionalLead.get();
        }
        throw new RuntimeException("Lead não encontrado.");

    }

    public Lead atualizarLead(int id, Lead lead) {
        if (leadRepository.existsById(id)) {
            lead.setId(id);
            Lead leadObjeto = salvarLead(lead);

            return leadObjeto;
        }

        throw new RuntimeException("Lead não encontrado.");
    }

    public void deletarLead(int id){
        if (leadRepository.existsById(id)){
            leadRepository.deleteById(id);
        } else {
            throw new RuntimeException("Lead não encontrado.");
        }
    }
}
