package br.com.itau.lead.collector.exceptions.errors;

public class ObjetoDeErro {
    private String mensagemErro;
    private String valorRejeitado;

    public ObjetoDeErro(){ }

    public ObjetoDeErro(String mensagemErro, String valorRejeitado) {
        this.mensagemErro = mensagemErro;
        this.valorRejeitado = valorRejeitado;
    }

    public String getMensagemDeErro() { return mensagemErro; }

    public void setMensagemDeErro(String mensagemDeErro) { this.mensagemErro = mensagemDeErro; }

    public String getValorRejeitado() { return valorRejeitado; }

    public void setValorRejeitado(String valorRejeitado) { this.valorRejeitado = valorRejeitado; }

}
