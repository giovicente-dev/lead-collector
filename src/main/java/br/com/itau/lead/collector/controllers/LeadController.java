package br.com.itau.lead.collector.controllers;

import br.com.itau.lead.collector.enums.TipoLeadEnum;
import br.com.itau.lead.collector.models.Lead;
import br.com.itau.lead.collector.services.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/leads")
public class LeadController {

    @Autowired
    private LeadService leadService;

    @PostMapping
    public ResponseEntity<Lead> registrarLead(@RequestBody @Valid Lead lead) {
        return ResponseEntity.status(201).body(leadService.salvarLead(lead));
    }

    @GetMapping
    public Iterable<Lead> exibirListaLeads(@RequestParam(name = "tipo_lead", required = false) TipoLeadEnum tipoLeadEnum) {
        if (tipoLeadEnum != null) {
            Iterable<Lead> leads = leadService.consultarListaPorTipoDeLead(tipoLeadEnum);
            return leads;
        }
        Iterable<Lead> leads = leadService.consultarListaLeads();
        return leads;
    }

    @GetMapping("/{id_lead}")
    public Lead exibirDetalhePorId(@PathVariable(name = "id_lead") int id) {
        try {
            Lead lead = leadService.consultarLeadPorId(id);
            return lead;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PutMapping("/{id_lead}")
    public Lead atualizarLeadPorId(@PathVariable(name = "id_lead") int id, @RequestBody Lead lead) {
        try {
            Lead leadObjeto = leadService.atualizarLead(id, lead);
            return leadObjeto;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @DeleteMapping("/{id_lead}")
    public ResponseEntity<?> deletarLeadPorId(@PathVariable(name = "id_lead") int id) {
        try {
            leadService.deletarLead(id);
            return ResponseEntity.status(204).body("");
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

}
