package br.com.itau.lead.collector.controllers;

import br.com.itau.lead.collector.models.Produto;
import br.com.itau.lead.collector.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @PostMapping
    public ResponseEntity<Produto> registrarProduto(@RequestBody Produto produto) {
        return ResponseEntity.status(201).body(produtoService.incluirProduto(produto));
    }

    @GetMapping
    public Iterable<Produto> consultarListaProdutos(@RequestParam(name = "nome", required = false) String nome) {
        if (nome != null) {
            Iterable<Produto> produtos = produtoService.listarProdutosPorNome(nome);
            return produtos;
        }

        Iterable<Produto> produtos = produtoService.listarProdutos();
        return produtos;
    }

    @GetMapping("/{id_produto}")
    public Produto consultarDetalheProduto(@PathVariable(name = "id_produto") int id) {
        try {
            Produto produto = produtoService.detalharProdutoPorId(id);
            return produto;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PutMapping("/{id_produto}")
    public Produto atualizarProdutoPorId(@PathVariable(name = "id_produto") int id, @RequestBody Produto produto) {
        try {
            Produto produtoObjeto = produtoService.atualizarProduto(id, produto);
            return produtoObjeto;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @DeleteMapping("/{id_produto}")
    public ResponseEntity<?> exluirProdutoPorId(@PathVariable(name = "id_produto") int id) {
        try {
            produtoService.deletarProduto(id);
            return ResponseEntity.status(204).body("");
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

}
