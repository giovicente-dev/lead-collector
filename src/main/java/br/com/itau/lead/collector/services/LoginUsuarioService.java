package br.com.itau.lead.collector.services;

import br.com.itau.lead.collector.models.Usuario;
import br.com.itau.lead.collector.repositories.UsuarioRepository;
import br.com.itau.lead.collector.security.LoginUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class LoginUsuarioService implements UserDetailsService {

    @Autowired
    UsuarioRepository usuarioRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Usuario usuario = usuarioRepository.findByEmail(email);
        if (usuario == null) {
            throw new UsernameNotFoundException(email);
        }
        LoginUsuario loginUsuario = new LoginUsuario(usuario.getId(), usuario.getEmail(), usuario.getSenha());
        return loginUsuario;
    }

}
