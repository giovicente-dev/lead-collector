package br.com.itau.lead.collector.repositories;

import br.com.itau.lead.collector.models.Produto;
import org.springframework.data.repository.CrudRepository;

public interface ProdutoRepository extends CrudRepository<Produto, Integer> {
    Iterable<Produto> findByNomeContains(String nome);
}
