package br.com.itau.lead.collector.enums;

public enum TipoLeadEnum {
    QUENTE,
    FRIO,
    ORGANICO
}
