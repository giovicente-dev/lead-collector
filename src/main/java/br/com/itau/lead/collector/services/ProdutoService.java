package br.com.itau.lead.collector.services;

import br.com.itau.lead.collector.models.Produto;
import br.com.itau.lead.collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    public Produto incluirProduto(Produto produto) {
        Produto produtoObjeto = produtoRepository.save(produto);
        return produtoObjeto;
    }

    public Iterable<Produto> listarProdutos() {
        Iterable<Produto> produtos = produtoRepository.findAll();
        return produtos;
    }

    public Iterable<Produto> listarProdutosPorNome(String nome){
        Iterable<Produto> produtos = produtoRepository.findByNomeContains(nome);
        return produtos;
    }

    public Produto detalharProdutoPorId(int id) {
        Optional<Produto> produtoOptional = produtoRepository.findById(id);

        if (produtoOptional.isPresent()) {
            return produtoOptional.get();
        }

        throw new RuntimeException("Produto não encontrado.");
    }

    public Produto atualizarProduto(int id, Produto produto) {
        if (produtoRepository.existsById(id)) {
            produto.setId(id);
            Produto produtoObjeto = incluirProduto(produto);

            return produto;
        }

        throw new RuntimeException("Produto não encontrado.");
    }

    public void deletarProduto(int id) {
        if (produtoRepository.existsById(id)) {
            produtoRepository.deleteById(id);
        } else {
            throw new RuntimeException("Produto não encontrado.");
        }
    }

}
