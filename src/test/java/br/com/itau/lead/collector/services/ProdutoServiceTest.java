package br.com.itau.lead.collector.services;

import br.com.itau.lead.collector.models.Produto;
import br.com.itau.lead.collector.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
public class ProdutoServiceTest {

    @MockBean
    private ProdutoRepository produtoRepository;

    @Autowired
    private ProdutoService produtoService;

    Produto produto;

    @BeforeEach
    public void setup() {
        produto = new Produto();
        produto.setId(1);
        produto.setNome("Treinamento XPTO");
        produto.setDescricao("Este treinamento ensina XPTO");
        produto.setPreco(297.00);
    }

    @Test
    public void testarIncluirProduto() {
        Mockito.when(produtoRepository.save(produto)).thenReturn(produto);
        Produto produtoTest = produtoService.incluirProduto(produto);

        Assertions.assertSame(produto, produtoTest);
    }

    @Test
    public void testarListarProdutos() {
        Iterable<Produto> produtos = Arrays.asList(produto);
        Mockito.when(produtoRepository.findAll()).thenReturn(produtos);

        Iterable<Produto> produtoIterable = produtoService.listarProdutos();

        Assertions.assertEquals(produtos, produtoIterable);
    }

    @Test
    public void testarListarProdutosPorNome() {
        Iterable<Produto> produtos = Arrays.asList(produto);
        Mockito.when(produtoRepository.findByNomeContains(produto.getNome())).thenReturn(produtos);

        Iterable<Produto> produtoIterable = produtoService.listarProdutosPorNome(produto.getNome());

        Assertions.assertEquals(produtos, produtoIterable);
    }

    @Test
    public void testarDetalharProdutoPorIdExistente() {
        Mockito.when(produtoRepository.findById(produto.getId())).thenReturn(Optional.of(produto));
        Produto produtoTest = produtoService.detalharProdutoPorId(produto.getId());

        Assertions.assertSame(produto, produtoTest);
    }

    @Test
    public void testarDetalharProdutoPorIdInexistente() {
        Mockito.when(produtoRepository.findById(produto.getId())).thenReturn(Optional.empty());
        Assertions.assertThrows(RuntimeException.class, () -> {produtoService.detalharProdutoPorId(Mockito.anyInt());});
    }


    @Test
    public void testarAtualizarProdutoPorIdExistente() {
        Produto produtoTeste = new Produto();
        produtoTeste.setId(1);
        produtoTeste.setNome("Treinamento XPTO parte 2");
        produtoTeste.setDescricao("Este treinamento ensina a segunda parte do XPTO");
        produtoTeste.setPreco(297.00);

        Mockito.when(produtoRepository.existsById(Mockito.anyInt())).thenReturn(true);
        Mockito.when(produtoService.incluirProduto(produtoTeste)).thenReturn(produtoTeste);

        Assertions.assertEquals(produto.getId(), produtoTeste.getId());
    }

    @Test
    public void testarAtualizarProdutoPorIdInexistente() {
        Mockito.when(produtoRepository.existsById(Mockito.anyInt())).thenReturn(false);
        Assertions.assertThrows(RuntimeException.class, () -> {produtoService.atualizarProduto(Mockito.anyInt(), produto);});
    }

    @Test
    public void testarDeletarProdutoPorIdExistente() {
        Mockito.when(produtoRepository.existsById(Mockito.anyInt())).thenReturn(true);
        produtoService.deletarProduto(Mockito.anyInt());
        Mockito.verify(produtoRepository, Mockito.times(1)).deleteById(Mockito.anyInt());
    }

    @Test
    public void testarDeletarProdutoPorIdInexistente() {
        Mockito.when(produtoRepository.existsById(produto.getId())).thenReturn(false);
        Assertions.assertThrows(RuntimeException.class, () -> {produtoService.deletarProduto(produto.getId());});
    }
}
