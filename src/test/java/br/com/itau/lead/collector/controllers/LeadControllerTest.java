package br.com.itau.lead.collector.controllers;

import br.com.itau.lead.collector.enums.TipoLeadEnum;
import br.com.itau.lead.collector.models.Lead;
import br.com.itau.lead.collector.models.Produto;
import br.com.itau.lead.collector.services.LeadService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@WebMvcTest(LeadController.class)
public class LeadControllerTest {

    @MockBean
    private LeadService leadService;

    @Autowired
    private MockMvc mockMvc;

    Lead lead;
    Produto produto;

    @BeforeEach
    public void setup() {
        lead = new Lead();
        lead.setNome("Giovanni");
        lead.setData(LocalDate.now());
        lead.setTipoLead(TipoLeadEnum.QUENTE);
        lead.setEmail("giovanni@gmail.com");

        produto = new Produto();
        produto.setNome("Treinamento XPTO");
        produto.setId(1);
        produto.setDescricao("Este treinamento ensina XPTO");

        List<Produto> produtos = new ArrayList<>();
        produtos.add(produto);
        lead.setProdutos(produtos);
    }

    @Test
    public void testarRegistrarLeadValido() throws Exception {
        Mockito.when(leadService.salvarLead(Mockito.any(Lead.class))).then(leadObjeto -> {
            lead.setId(1);
            return lead;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonLead = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
               .contentType(MediaType.APPLICATION_JSON)
               .content(jsonLead))
               .andExpect(MockMvcResultMatchers.status().isCreated())
               .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarExibirListaLeads() throws Exception {
        ArrayList<Lead> leads = new ArrayList<>();
        leads.add(lead);

        Iterable<Lead> leadsIterable = leads;

        Mockito.when(leadService.consultarListaLeads()).thenReturn(leadsIterable);

        ObjectMapper mapper = new ObjectMapper();
        String jsonIterableLead = mapper.writeValueAsString(leadsIterable);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads")
               .contentType(MediaType.APPLICATION_JSON))
               .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
