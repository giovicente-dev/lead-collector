package br.com.itau.lead.collector.services;

import br.com.itau.lead.collector.enums.TipoLeadEnum;
import br.com.itau.lead.collector.models.Lead;
import br.com.itau.lead.collector.models.Produto;
import br.com.itau.lead.collector.repositories.LeadRepository;
import br.com.itau.lead.collector.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.doAnswer;

@SpringBootTest
public class LeadServiceTest {

    @MockBean
    private LeadRepository leadRepository;

    @Autowired
    private LeadService leadService;

    Lead lead;
    Produto produto;

    @BeforeEach
    public void setup() {
        lead = new Lead();
        lead.setId(1);
        lead.setNome("Giovanni");
        lead.setData(LocalDate.now());
        lead.setTipoLead(TipoLeadEnum.QUENTE);
        lead.setEmail("giovanni@gmail.com");

        produto = new Produto();
        produto.setNome("Treinamento XPTO");
        produto.setId(1);
        produto.setDescricao("Este treinamento ensina XPTO");

        List<Produto> produtos = new ArrayList<>();
        produtos.add(produto);
        lead.setProdutos(produtos);
    }

    @Test
    public void testarSalvarLead() {
        Mockito.when(leadRepository.save(lead)).thenReturn(lead);
        Lead leadTest = leadService.salvarLead(lead);

        Assertions.assertEquals(LocalDate.now(), leadTest.getData());
        Assertions.assertSame(lead, leadTest);
    }

    @Test
    public void testarConsultarListaLeads() {
        Iterable<Lead> leads = Arrays.asList(lead);
        Mockito.when(leadRepository.findAll()).thenReturn(leads);

        Iterable<Lead> leadIterable = leadService.consultarListaLeads();

        Assertions.assertEquals(leads, leadIterable);
    }

    @Test
    public void testarConsultarListaPorTipoDeLead() {
        Iterable<Lead> leads = Arrays.asList(lead);
        Mockito.when(leadRepository.findAllByTipoLead(lead.getTipoLead())).thenReturn(leads);

        Iterable<Lead> leadIterable = leadService.consultarListaPorTipoDeLead(lead.getTipoLead());

        Assertions.assertEquals(leads, leadIterable);
    }

    @Test
    public void testarConsultarLeadPorIdComRegistroExistente() {
        Mockito.when(leadRepository.findById(lead.getId())).thenReturn(Optional.of(lead));

        Lead leadTest = leadService.consultarLeadPorId(lead.getId());

        Assertions.assertSame(lead, leadTest);
    }

    @Test
    public void testarConsultarLeadPorIdComRegistroInexistente() {
        Mockito.when(leadRepository.findById(lead.getId())).thenReturn(Optional.empty());

        Assertions.assertThrows(RuntimeException.class, () -> {leadService.consultarLeadPorId(lead.getId());});
    }

    @Test
    public void testarAtualizarLeadComRegistroExistente() {
        Lead leadTeste = new Lead();

        leadTeste.setId(1);
        leadTeste.setNome("Giovanni Vicente");
        leadTeste.setData(LocalDate.now());
        leadTeste.setTipoLead(TipoLeadEnum.QUENTE);
        leadTeste.setEmail("gvn.gentile@gmail.com");

        produto = new Produto();
        produto.setNome("Treinamento XPTO");
        produto.setId(1);
        produto.setDescricao("Este treinamento ensina XPTO");

        List<Produto> produtos = new ArrayList<>();
        produtos.add(produto);
        leadTeste.setProdutos(produtos);

        Mockito.when(leadRepository.existsById(Mockito.anyInt())).thenReturn(true);
        Mockito.when(leadService.salvarLead(leadTeste)).thenReturn(leadTeste);

        Assertions.assertEquals(lead.getId(), leadTeste.getId());

    }

    @Test
    public void testarAtualizarLeadComRegistroInexistente() {
        Mockito.when(leadRepository.existsById(Mockito.anyInt())).thenReturn(false);
        Assertions.assertThrows(RuntimeException.class, () -> {leadService.atualizarLead(Mockito.anyInt(), lead);});
    }

    @Test
    public void testarDeletarLeadComRegistroExistente() {
        Mockito.when(leadRepository.existsById(Mockito.anyInt())).thenReturn(true);
        leadService.deletarLead(Mockito.anyInt());
        Mockito.verify(leadRepository, Mockito.times(1)).deleteById(Mockito.anyInt());
    }

    @Test
    public void testarDeletarLeadComRegistroInexistente() {
        Mockito.when(leadRepository.existsById(Mockito.anyInt())).thenReturn(false);
        Assertions.assertThrows(RuntimeException.class, () -> {leadService.deletarLead(lead.getId());});
    }
}
